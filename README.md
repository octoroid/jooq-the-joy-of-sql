# jooq-the-joy-of-sql

## Setup

### Launch Database:

```shell script
docker run --rm -it \
      -e POSTGRES_PASSWORD=jooq \
      -e POSTGRES_USER=jooq \
      -e POSTGRES_DB=jooq \
      -p 5432:5432 \
      -v $PWD/database/:/docker-entrypoint-initdb.d/ \
      --name postgres postgres:12.3-alpine
```

