package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.UUID;

/**
 * Created by kevin on 11/07/2020
 */
public record Podcast(UUID id, String title, String url, JsonNode metadata) {}
