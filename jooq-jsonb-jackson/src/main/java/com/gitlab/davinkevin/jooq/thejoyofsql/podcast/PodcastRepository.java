package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import org.jooq.DSLContext;

import java.util.Optional;
import java.util.UUID;

import static com.gitlab.davinkevin.podcastserver.database.Tables.PODCAST;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

/**
 * Created by kevin on 11/07/2020
 */
public class PodcastRepository {

    private final DSLContext query;

    public PodcastRepository(DSLContext query) {
        this.query = query;
    }

    public Optional<Podcast> findOne(UUID id) {
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL, PODCAST.METADATA)
                .from(PODCAST)
                .where(PODCAST.ID.eq(id))
                .orderBy(PODCAST.ID.asc())
                .fetchOptional()
                .map(it -> new Podcast(
                        it.get(PODCAST.ID),
                        it.get(PODCAST.TITLE),
                        it.get(PODCAST.URL),
                        it.get(PODCAST.METADATA)
                    )
                );
    }
}

