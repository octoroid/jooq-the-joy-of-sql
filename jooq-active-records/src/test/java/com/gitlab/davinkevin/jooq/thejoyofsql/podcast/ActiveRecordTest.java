package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import com.gitlab.davinkevin.podcastserver.database.tables.records.PodcastRecord;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;

import static com.gitlab.davinkevin.podcastserver.database.Tables.PODCAST;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by kevin on 18/07/2020
 */
public class ActiveRecordTest {

    private static Connection connection;
    private DSLContext query;

    @BeforeAll
    public static void beforeAll() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq","jooq");
    }

    @BeforeEach
    public void beforeEach() {
        query = DSL.using(connection, SQLDialect.POSTGRES);
    }

    @Test
    public void should_fetch_one() {
        /* GIVEN */
        var id = UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d");
        /* WHEN  */
        var podcast = query.newRecord(PODCAST);
        podcast.setId(id);
        podcast.refresh();
        /* THEN  */
        assertThat(podcast.getTitle()).isEqualTo("Les Cast Codeurs");
        assertThat(podcast.getId()).isEqualTo(id);
        assertThat(podcast.getDescription()).isNull();
    }

    @Test
    public void should_update_too() {
        /* GIVEN */
        var id = UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d");
        var podcast = query.newRecord(PODCAST);
        podcast.setId(id);
        podcast.refresh();
        /* WHEN  */
        podcast.setDescription("Le meilleur podcast sur la JVM 🇫🇷");
        podcast.update();
        /* THEN  */
        assertThat(podcast.getTitle()).isEqualTo("Les Cast Codeurs");
        assertThat(podcast.getDescription()).isNotNull().isEqualTo("Le meilleur podcast sur la JVM 🇫🇷");
    }
}
