package com.gitlab.davinkevin.jooq.thejoyofsql.page;

import java.util.Collection;

import static java.lang.Math.ceil;

/**
 * Created by kevin on 26/07/2020
 */
public record Page<T>(
        Collection<T> content,
        Boolean empty,
        Boolean first,
        Boolean last,
        Integer number,
        Integer numberOfElements,
        Integer size,
        Integer totalElements,
        Integer totalPages
) {
    public static <T> Page<T> of(
            Collection<T> content,
            Integer totalElements,
            PageRequest page
    ) {
       var totalPages = (int) ceil(totalElements.doubleValue() / page.size().doubleValue());
       return new Page<>(
               content,
               content.isEmpty(),
               page.page() == 0,
               page.page() + 1 > totalPages - 1,
               page.page(),
               content.size(),
               page.size(),
               totalElements,
               totalPages
       );
    }
}
