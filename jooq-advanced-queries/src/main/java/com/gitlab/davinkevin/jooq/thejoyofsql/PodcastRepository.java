package com.gitlab.davinkevin.jooq.thejoyofsql;

import com.gitlab.davinkevin.jooq.thejoyofsql.stats.NumberOfItemByDate;
import com.gitlab.davinkevin.jooq.thejoyofsql.stats.StatsPodcastByType;
import org.jooq.DSLContext;
import org.jooq.Record3;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.davinkevin.podcastserver.database.Tables.ITEM;
import static com.gitlab.davinkevin.podcastserver.database.Tables.PODCAST;
import static java.util.stream.Collectors.*;
import static org.jooq.impl.DSL.*;

/**
 * Created by kevin on 18/07/2020
 */
public class PodcastRepository {

    private final DSLContext query;

    public PodcastRepository(DSLContext query) {
        this.query = query;
    }

    public List<StatsPodcastByType> findStatByTypeAndPubDate(OffsetDateTime startingFrom) {
        var date = trunc(ITEM.PUB_DATE);

        return query
                .select(
                        PODCAST.TYPE,
                        date,
                        count()
                )
                .from(ITEM.innerJoin(PODCAST).on(ITEM.PODCAST_ID.eq(PODCAST.ID)))
                .where(ITEM.PUB_DATE.isNotNull())
                .and(
                        date.gt(offsetDateTime(startingFrom).minus(10))
                )
                .groupBy(PODCAST.TYPE, date)
                .orderBy(PODCAST.TYPE, date)
                .fetch()
                // Result<Record3<PODCAST.TYPE, count(), OffsetDateTime>>
                // It looks like: [
                // 		Tuple(“Youtube”, 10, 2020-11-17),
                // 		Tuple(“Youtube”, 15, 2020-11-16),
                // 		Tuple(“RSS”, 15, 2020-11-17),
                //		…
                //]
                .stream()
                .collect(
                        groupingBy(
                                Record3::value1,
                                mapping(
                                        it -> new NumberOfItemByDate(it.value2().toLocalDate(), it.value3()),
                                        toList()
                                )
                        )
                )
                .entrySet()
                .stream()
                .map(it -> new StatsPodcastByType(it.getKey(), it.getValue()))
                .collect(toList());
    }

    public List<StatsPodcastByType> findStatByTypeAndPubDateCumulated(OffsetDateTime startingFrom) {
        var date = trunc(ITEM.PUB_DATE);
        return query
                .select(
                        PODCAST.TYPE,
                        date,
                        sum(count()).over()
                                .partitionBy(PODCAST.TYPE)
                                .orderBy(date)
                                .rowsBetweenUnboundedPreceding()
                                .andCurrentRow()
                )
                .from(ITEM.innerJoin(PODCAST).on(ITEM.PODCAST_ID.eq(PODCAST.ID)))
                .where(ITEM.PUB_DATE.isNotNull())
                .and(
                        date.gt(offsetDateTime(startingFrom).minus(10))
                )
                .groupBy(PODCAST.TYPE, date)
                .orderBy(PODCAST.TYPE, date)
                .fetch()
                // Record<PODCAST.TYPE, count(), OffsetDateTime>
                .stream()
                .collect(
                        groupingBy(
                                Record3::value1,
                                mapping(
                                        it -> new NumberOfItemByDate(it.value2().toLocalDate(), it.value3().intValue()),
                                        toList()
                                )
                        )
                )
                .entrySet()
                .stream()
                .map(it -> new StatsPodcastByType(it.getKey(), it.getValue()))
                .collect(Collectors.toList());
    }

//    public List<StatsPodcastByType> _findStatByTypeAndPubDateCumulated(OffsetDateTime startingFrom) {
//        var date = trunc(ITEM.PUB_DATE);
//        return query
//                .select(
//                        PODCAST.TYPE,
//                        date,
//                        sum(count()).over()
//                                .partitionBy(PODCAST.TYPE)
//                                .orderBy(date)
//                                .rowsBetweenUnboundedPreceding()
//                                .andCurrentRow()
//                )
//                .from(ITEM.innerJoin(PODCAST).on(ITEM.PODCAST_ID.eq(PODCAST.ID)))
//                .where(ITEM.PUB_DATE.isNotNull())
//                .and(
//                        date.gt(offsetDateTime(startingFrom).minus(10))
//                )
//                .groupBy(PODCAST.TYPE, date)
//                .orderBy(PODCAST.TYPE, date)
//                // Record<PODCAST.TYPE, count(), OffsetDateTime>
//                .fetch(it -> convertToStats(it));
//    }

}
