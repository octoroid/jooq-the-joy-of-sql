package com.gitlab.davinkevin.jooq.thejoyofsql.stats;

import java.time.LocalDate;

public record NumberOfItemByDate(
        LocalDate date,
        Integer numberOfItems
) {}
