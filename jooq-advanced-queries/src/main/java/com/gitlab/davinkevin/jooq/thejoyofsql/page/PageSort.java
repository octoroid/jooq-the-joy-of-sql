package com.gitlab.davinkevin.jooq.thejoyofsql.page;

/**
 * Created by kevin on 26/07/2020
 */
public record PageSort(
        String direction,
        String field
){}
