package com.gitlab.davinkevin.jooq.thejoyofsql.page;

/**
 * Created by kevin on 26/07/2020
 */
public record PageRequest(
        Integer page,
        Integer size,
        PageSort sort
) {}
