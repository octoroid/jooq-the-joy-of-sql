package com.gitlab.davinkevin.jooq.thejoyofsql.page.item;

/**
 * Created by kevin on 26/07/2020
 */
public enum Status {
    NOT_DOWNLOADED,
    STARTED,
    PAUSED,
    DELETED,
    STOPPED,
    FAILED,
    FINISH
}
