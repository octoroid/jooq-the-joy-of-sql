package com.gitlab.davinkevin.jooq.thejoyofsql;

import com.gitlab.davinkevin.jooq.thejoyofsql.stats.NumberOfItemByDate;
import com.gitlab.davinkevin.jooq.thejoyofsql.stats.StatsPodcastByType;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderQuotedNames;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by kevin on 18/07/2020
 */
class PodcastRepositoryTest {

    private static Connection connection;
    private PodcastRepository repository;

    private final OffsetDateTime starting = OffsetDateTime.of(
            2020, 7, 1,
            0, 0, 0, 0,
            ZoneOffset.UTC
    );

    @BeforeAll
    public static void beforeAll() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq","jooq");
    }

    @BeforeEach
    public void beforeEach() {
        var query = DSL.using(connection, SQLDialect.POSTGRES
                , new Settings()
                        .withRenderFormatted(true)
                        .withRenderSchema(false)
                        .withRenderQuotedNames(RenderQuotedNames.NEVER)
        );
        repository = new PodcastRepository(query);
    }

    @Test
    public void should_find_stats_since_the_beginning() {
        /* GIVEN */
        var expectedRssResult = new StatsPodcastByType("RSS", List.of(
                new NumberOfItemByDate(LocalDate.of(2020, 6, 22),   2),
//                new NumberOfItemByDate(LocalDate.of(2020, 6, 23),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 24),   3),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 26),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 27),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 29),   3),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 30),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 1),    3),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 3),    1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 4),    1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 6),    3),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 7),    1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 8),    3),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 10),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 11),   1)
        ));
        var expectedYoutubeResults = new StatsPodcastByType("Youtube", List.of(
                new NumberOfItemByDate(LocalDate.of(2020, 6, 22),   9),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 23),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 24),   16),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 25),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 26),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 27),   2),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 29),   1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 1),    2),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 2),    1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 3),    13),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 6),    1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 9),    1),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 10),   1)
        ));

        /* WHEN  */
        var stats = repository.findStatByTypeAndPubDate(starting);

        /* THEN  */
        assertThat(stats).hasSize(2);
        assertThat(stats.get(0)).isEqualTo(expectedRssResult);
        assertThat(stats.get(1)).isEqualTo(expectedYoutubeResults);
    }

    @Test
    public void should_find_stats_since_the_beginning_cumulated() {
        /* GIVEN */
        var expectedRssResult = new StatsPodcastByType("RSS", List.of(
                new NumberOfItemByDate(LocalDate.of(2020, 6, 22),   2),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 24),   5),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 26),   6),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 27),   7),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 29),   10),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 30),   11),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 1),    14),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 3),    15),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 4),    16),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 6),    19),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 7),    20),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 8),    23),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 10),   24),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 11),   25)
        ));
        var expectedYoutubeResults = new StatsPodcastByType("Youtube", List.of(
                new NumberOfItemByDate(LocalDate.of(2020, 6, 22),   9),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 23),   10),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 24),   26),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 25),   27),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 26),   28),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 27),   30),
                new NumberOfItemByDate(LocalDate.of(2020, 6, 29),   31),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 1),    33),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 2),    34),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 3),    47),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 6),    48),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 9),    49),
                new NumberOfItemByDate(LocalDate.of(2020, 7, 10),   50)
        ));

        /* WHEN  */
        var stats = repository.findStatByTypeAndPubDateCumulated(starting);

        /* THEN  */
        assertThat(stats).hasSize(2);
        assertThat(stats.get(0)).isEqualTo(expectedRssResult);
        assertThat(stats.get(1)).isEqualTo(expectedYoutubeResults);
    }

}
