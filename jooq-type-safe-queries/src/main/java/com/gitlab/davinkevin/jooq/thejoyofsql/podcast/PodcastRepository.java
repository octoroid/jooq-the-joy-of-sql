package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import org.jooq.DSLContext;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.gitlab.davinkevin.podcastserver.database.Tables.COVER;
import static com.gitlab.davinkevin.podcastserver.database.Tables.PODCAST;

/**
 * Created by kevin on 11/07/2020
 */
public record PodcastRepository(DSLContext query) {

    public List<Podcast> findAll() {
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL)
                .from(PODCAST)
                .orderBy(PODCAST.ID.asc())
                .fetch(it -> new Podcast(it.get(PODCAST.ID), it.get(PODCAST.TITLE), it.get(PODCAST.URL)));
    }

    public Optional<Podcast> findOne(UUID id) {
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL)
                .from(PODCAST)
                .where(PODCAST.ID.eq(id))
                .fetchOptional(it -> new Podcast(it.get(PODCAST.ID), it.get(PODCAST.TITLE), it.get(PODCAST.URL)));
    }

    public List<PodcastWithCover> findThreeWithCover() {
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL, COVER.URL, COVER.HEIGHT, COVER.WIDTH)
                .from(PODCAST)
                .innerJoin(COVER).on(PODCAST.COVER_ID.eq(COVER.ID))
                .orderBy(PODCAST.ID.asc())
                .limit(3)
                .fetch(it -> {
                    var cover = new PodcastWithCover.Cover(it.get(COVER.URL), it.get(COVER.HEIGHT), it.get(COVER.WIDTH));
                    return new PodcastWithCover(it.get(PODCAST.ID), it.get(PODCAST.TITLE), it.get(PODCAST.URL), cover);
                });
    }

    public List<PodcastWithCover> findAllWithCoverSimplified() {
        var coverUrl = PODCAST.cover().URL;
        var coverHeight = PODCAST.cover().HEIGHT;
        var coverWidth = PODCAST.cover().WIDTH;

        return query
                .select(
                        PODCAST.ID, PODCAST.TITLE, PODCAST.URL,
                        coverUrl, coverHeight, coverWidth
                )
                .from(PODCAST)
                .orderBy(PODCAST.ID.asc())
                .limit(3)
                .fetch(it -> {
                    var cover = new PodcastWithCover.Cover(it.get(coverUrl), it.get(coverHeight), it.get(coverWidth));
                    return new PodcastWithCover(it.get(PODCAST.ID), it.get(PODCAST.TITLE), it.get(PODCAST.URL), cover);
                });
    }

    public Podcast create(Podcast p) {
        var id = UUID.randomUUID();
//        query
//                .insertInto(PODCAST, PODCAST.ID, PODCAST.TITLE, PODCAST.URL)
//                .values(id, p.title(), p.url())
//                .execute();
        query
                .insertInto(PODCAST)
                .set(PODCAST.ID, id)
                .set(PODCAST.TITLE, p.title())
                .set(PODCAST.URL, p.url())
                .onConflict(PODCAST.URL).doUpdate()
                .set(PODCAST.LAST_UPDATE, OffsetDateTime.now())
                .execute();

        return new Podcast(id, p.title(), p.url());
    }

}

