package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import java.util.UUID;

public record PodcastWithCover(UUID id, String title, String url, Cover cover) {
    static public record Cover(String url, Integer height, Integer width) {}
}
