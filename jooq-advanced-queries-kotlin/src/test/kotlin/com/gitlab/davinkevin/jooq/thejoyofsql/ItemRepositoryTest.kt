package com.gitlab.davinkevin.jooq.thejoyofsql

import com.gitlab.davinkevin.jooq.thejoyofsql.page.PageRequest
import com.gitlab.davinkevin.jooq.thejoyofsql.page.Sort
import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Item
import org.assertj.core.api.Assertions.assertThat
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestInstance.Lifecycle
import java.sql.Connection
import java.sql.DriverManager
import java.util.*

/**
 * Created by kevin on 26/07/2020
 */
@TestInstance(value = Lifecycle.PER_CLASS)
class ItemRepositoryTest {

    private lateinit var connection: Connection
    private lateinit var repository: ItemRepository

    @BeforeAll
    fun beforeAll() {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq", "jooq")
    }

    @BeforeEach
    fun beforeEach() {
        val query = DSL.using(connection, SQLDialect.POSTGRES /*, new Settings().withRenderFormatted(true)*/)
        repository = ItemRepository(query)
    }

    @Test
    fun should_search_for_items() {
        /* GIVEN */
        val request = PageRequest(0, 5, Sort("desc", "pubDate"))

        /* WHEN  */
        val result = repository.search(
                page = request
        )

        /* THEN  */
        assertThat(result.content)
                .hasSize(5)
                .extracting<String> { it.title }
                .containsExactly(
                        "Google Cloud Run avec Steren Giannini",
                        "Episode 4 – What’s Coming in PhpStorm 2020.2 – EAP Series",
                        "#44.exe vu par Jacques Ducloy – Le streaming aussi pour du texte – Valentin Baudot",
                        "Message à caractère informatique #6 - Déployer le service mesh du mandalorian sur Rancher",
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf"
                )
        assertThat(result.first).isTrue()
        assertThat(result.last).isFalse()
        assertThat(result.totalElements).isEqualTo(2039)
        assertThat(result.totalPages).isEqualTo(408)
    }

    @Test
    fun should_search_for_items_with_tag_and_status_and_no_podcast() {
        /* GIVEN */
        val request = PageRequest(0, 5, Sort("desc", "pubDate"))

        /* WHEN  */
        val result = repository.search(
                tagNames = listOf("Java"),
                page = request
        )

        /* THEN  */
        assertThat<Item>(result.content)
                .hasSize(5)
                .extracting<String> { it.title }
                .containsExactly(
                        "Episode 4 – What’s Coming in PhpStorm 2020.2 – EAP Series",
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf",
                        "Profiling and Dynamic Program Analysis in Rider",
                        "Getting Started with Rider for #UnrealEngine",
                        "Customize the Look and Feel - Rider Essentials"
                )
        assertThat(result.first).isTrue()
        assertThat(result.last).isFalse()
        assertThat(result.totalElements).isEqualTo(777)
        assertThat(result.totalPages).isEqualTo(156)
    }

    @Test
    fun should_search_for_items_with_status_and_downloaded_with_no_podcast() {
        /* GIVEN */
        val request = PageRequest(0, 5, Sort("desc", "pubDate"))

        /* WHEN  */
        val result = repository.search(
                tagNames = listOf("Java"),
                statuses = listOf("FINISH"),
                page = request
        )

        /* THEN  */
        assertThat<Item>(result.content)
                .hasSize(4)
                .extracting<String> { it.title }
                .containsExactly(
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf",
                        "LCC 234 - EmmanuelBernard-As-A-Service, bientôt chez vous !",
                        "LCC 233 - Interview sur l'Event Storming avec Thomas Pierrain et Bruno Boucard",
                        "LCC 232 - Versions version Sloubi"
                )
        assertThat(result.first).isTrue()
        assertThat(result.last).isTrue()
        assertThat(result.totalElements).isEqualTo(4)
        assertThat(result.totalPages).isEqualTo(1)
    }

    @Test
    fun should_search_for_items_in_podcast() {
        /* GIVEN */
        val request = PageRequest(0, 5, Sort("desc", "pubDate"))

        /* WHEN  */
        val result = repository.search(
                page = request,
                podcastId = UUID.fromString("b08c7404-d9de-43d0-b1a3-25b8a26fa67d")
        )

        /* THEN  */
        assertThat<Item>(result.content)
                .hasSize(5)
                .extracting<String> { it.title }
                .containsExactly(
                        "LCC 235 - Interview Micro Services avec @ygrenzinger et @khaledsouf",
                        "LCC 234 - EmmanuelBernard-As-A-Service, bientôt chez vous !",
                        "LCC 233 - Interview sur l'Event Storming avec Thomas Pierrain et Bruno Boucard",
                        "LCC 232 - Versions version Sloubi",
                        "LCC 231 - Interview sur Vim avec Romain Lafourcade"
                )
        assertThat(result.first).isTrue()
        assertThat(result.last).isFalse()
        assertThat(result.totalElements).isEqualTo(235)
        assertThat(result.totalPages).isEqualTo(47)
    }

    @Test
    fun should_search_for_items_with_kotlin() {
        /* GIVEN */
        val request = PageRequest(0, 5, Sort("desc", "pubDate"))

        /* WHEN  */
        val result = repository.search(
                q = "kotlin",
                page = request
        )

        /* THEN  */
        assertThat<Item>(result.content)
                .hasSize(5)
                .extracting<String> { it.title }
                .containsExactly(
                        "QLDB",
                        """Présentation de Jetpack  "Compose" ton p'tit déj avec Romain Guy""",
                        "[DevFest du Bout du Monde 2020] Une décennie d'exploitation de vos applications dans le Cloud ave...",
                        "[DevFest du Bout du Monde 2020] L'OSS en entreprise, causons peu, Koson bien !",
                        "Amplify your web and mobile apps development. Full stack, cloud-powered."
                )
        assertThat(result.first).isTrue()
        assertThat(result.last).isFalse()
        assertThat(result.totalElements).isEqualTo(303)
        assertThat(result.totalPages).isEqualTo(61)
    }

}
