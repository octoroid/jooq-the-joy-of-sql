package com.gitlab.davinkevin.jooq.thejoyofsql.page.item

import java.net.URI
import java.time.OffsetDateTime
import java.util.*

/**
 * Created by kevin on 26/07/2020
 */

data class Item(
        val id: UUID,
        val title: String,
        val url: String?,

        val pubDate: OffsetDateTime?,
        val downloadDate: OffsetDateTime?,
        val creationDate: OffsetDateTime?,

        val description: String?,
        val mimeType: String,
        val length: Long?,
        val fileName: String?,
        val status: Status,

        val podcast: Podcast,
        val cover: Cover
) {
    data class Podcast(val id: UUID, val title: String, val url: String?)
    data class Cover(val id: UUID, val url: URI, val width: Int, val height: Int)
}

enum class Status {
    NOT_DOWNLOADED,
    STARTED,
    PAUSED,
    DELETED,
    STOPPED,
    FAILED,
    FINISH;
}
