package com.gitlab.davinkevin.jooq.thejoyofsql.page

import com.gitlab.davinkevin.jooq.thejoyofsql.page.item.Item
import kotlin.math.ceil

/**
 * Created by kevin on 26/07/2020
 */
data class Page<T>(
        val content: Collection<T>,
        val empty: Boolean,
        val first: Boolean,
        val last: Boolean,
        val number: Int,
        val numberOfElements: Int,
        val size: Int,
        val totalElements: Int,
        val totalPages: Int
) {
    companion object {

        fun <T> of(content: Collection<T>, totalElements: Int, page: PageRequest): Page<T> {

            val totalPages = ceil(totalElements.toDouble() / page.size.toDouble()).toInt()

            return Page(
                    content = content,
                    empty = content.isEmpty(),
                    first = page.page == 0,
                    last = page.page + 1 > totalPages - 1,
                    number = page.page,
                    numberOfElements = content.size,
                    size = page.size,
                    totalElements = totalElements,
                    totalPages = totalPages
            )
        }

    }
}

data class Sort(val direction: String, val field: String)
data class PageRequest(val page: Int, val size: Int, val sort: Sort)
