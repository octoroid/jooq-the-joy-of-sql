package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import org.jooq.DSLContext;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

/**
 * Created by kevin on 11/07/2020
 */
public class PodcastRepository {

    private final DSLContext query;

    public PodcastRepository(DSLContext query) {
        this.query = query;
    }

    /*
        SELECT PODCAST.ID, PODCAST.TITLE, PODCAST.URL
        FROM PODCAST
        ORDER BY PODCAST.ID ASC
    */
    public List<Podcast> findAll() {

        var id = field("PODCAST.ID").cast(UUID.class);
        var title = field("PODCAST.TITLE").cast(String.class);
        var url = field("PODCAST.URL").cast(String.class);

        return query
                .select(id, title, url)
                .from(table("PODCAST"))
                .orderBy(id.asc())
                .fetch(it -> new Podcast(it.get(id), it.get(title), it.get(url)));
    }

    /*
      SELECT TITLE, URL
      FROM PODCAST
      WHERE ID == :ID
    */
    public Optional<Podcast> findOneById(UUID id) {
        var _id = field("PODCAST.ID").cast(UUID.class);
        var title = field("PODCAST.TITLE").cast(String.class);
        var url = field("PODCAST.URL").cast(String.class);

        return query
                .select(title, url)
                .from(table("PODCAST"))
                .where(_id.eq(id))
                .fetchOptional(it -> new Podcast(id, it.get(title), it.get(url)));
    }

}
