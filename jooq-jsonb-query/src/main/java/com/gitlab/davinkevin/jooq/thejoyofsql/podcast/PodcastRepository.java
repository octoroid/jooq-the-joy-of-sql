package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import org.jooq.DSLContext;
import org.jooq.Field;
import org.jooq.JSONB;
import org.jooq.QueryPart;
import org.jooq.impl.DSL;

import java.util.Optional;
import java.util.UUID;

import static com.github.t9t.jooq.json.JsonbDSL.*;
import static com.gitlab.davinkevin.podcastserver.database.Tables.PODCAST;
import static org.jooq.impl.DSL.field;
import static org.jooq.impl.DSL.table;

/**
 * Created by kevin on 11/07/2020
 */
public class PodcastRepository {

    private final DSLContext query;

    public PodcastRepository(DSLContext query) {
        this.query = query;
    }

    public Optional<YoutubePodcast> findOne(UUID id) {
        var youtubeField = jsonPath(PODCAST.METADATA, "youtube", "channelId");
        return query
                .select(PODCAST.ID, PODCAST.TITLE, PODCAST.URL, youtubeField)
                .from(PODCAST)
                .where(PODCAST.ID.eq(id))
                .orderBy(PODCAST.ID.asc())
                .fetchOptional()
                .map(it -> new YoutubePodcast(
                                it.get(PODCAST.ID),
                                it.get(PODCAST.TITLE),
                                it.get(PODCAST.URL),
                                it.get(youtubeField)
                        )
                );
    }

    private static Field<String> jsonPath(Field<JSONB> jsonField, String... path) {
        return DSL.field("{0}#>>{1}", String.class, jsonField, DSL.array(path));
    }
}

