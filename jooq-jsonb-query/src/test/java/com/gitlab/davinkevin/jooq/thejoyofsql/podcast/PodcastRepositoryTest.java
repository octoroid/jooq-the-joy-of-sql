package com.gitlab.davinkevin.jooq.thejoyofsql.podcast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.assertj.core.api.Assertions;
import org.jooq.SQLDialect;
import org.jooq.conf.RenderQuotedNames;
import org.jooq.conf.Settings;
import org.jooq.impl.DSL;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.UUID;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by kevin on 11/07/2020
 */
class PodcastRepositoryTest {

    private static Connection connection;
    private final ObjectMapper mapper = new ObjectMapper();
    private PodcastRepository repository;

    @BeforeAll
    public static void beforeAll() throws SQLException {
        connection = DriverManager.getConnection("jdbc:postgresql://postgres:5432/jooq", "jooq","jooq");
    }

    @BeforeEach
    public void beforeEach() {
        var query = DSL.using(connection, SQLDialect.POSTGRES
                , new Settings()
                        .withRenderFormatted(true)
                        .withRenderSchema(false)
                        .withRenderQuotedNames(RenderQuotedNames.NEVER)
        );
        repository = new PodcastRepository(query);
    }

    @Test
    public void should_find_all_podcasts() throws JsonProcessingException {
        /* GIVEN */
        var id = UUID.fromString("2e8a6a65-d1ab-4128-b355-edd5e11bddee");
        /* WHEN  */
        var podcast = repository.findOne(id);
        /* THEN  */
        assertThat(podcast).isPresent().contains(
                new YoutubePodcast(
                        UUID.fromString("2e8a6a65-d1ab-4128-b355-edd5e11bddee"),
                        "JUG Leaders",
                        "https://www.youtube.com/channel/UCmq9YQtlwsew4rAeO3H-ZLg/videos",
                        "UC_QE5QFEQ6E45GH3AC5"
                )
        );
    }

}
