package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.jpa.advanced;

import javax.persistence.*;
import java.util.UUID;

/**
 * Created by kevin on 23/08/2020
 */
@Entity
@Table(name = "ITEM")
public class Item {
    @Id
    @GeneratedValue
    @Column(columnDefinition = "UUID")
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    private Podcast podcast;

    public UUID getId() {
        return id;
    }
    public void setId(UUID id) {
        this.id = id;
    }
    public Podcast getPodcast() {
        return podcast;
    }
    public void setPodcast(Podcast podcast) {
        this.podcast = podcast;
    }
}
