package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.ejb;

import java.util.UUID;

/**
 * Created by kevin on 22/08/2020
 */
public interface PodcastRequest extends EJBHome{
    Podcast create(UUID id);
    Podcast find(UUID id);
}
