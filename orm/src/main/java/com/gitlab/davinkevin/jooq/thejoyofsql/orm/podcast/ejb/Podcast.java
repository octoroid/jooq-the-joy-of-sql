package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.ejb;

import java.util.UUID;

/**
 * Created by kevin on 22/08/2020
 */
public interface Podcast extends EJBObject {
    UUID getId();
    String getTitle();
    String getUrl();

    @Override
    void remove();
}

