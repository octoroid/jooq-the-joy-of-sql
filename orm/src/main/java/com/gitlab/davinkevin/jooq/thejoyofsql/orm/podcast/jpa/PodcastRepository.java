package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.jpa;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.*;

/**
 * Created by kevin on 22/08/2020
 */
public record PodcastRepository(EntityManager em) {

    public List<Podcast> findAll() {
        return em.createQuery("select p FROM Podcast p", Podcast.class).getResultList();
    }

    public Optional<Podcast> findOneByIdWithItems(UUID id) {
        EntityGraph entityGraph = em.getEntityGraph("podcast-item-graph");
        Map<String, Object> properties = new HashMap<>();
        properties.put("javax.persistence.fetchgraph", entityGraph);
        Podcast podcast = em.find(Podcast.class, id, properties);

        return Optional.ofNullable(podcast);
    }

    public Optional<Podcast> findOneById(UUID id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();

        CriteriaQuery<Podcast> query = cb.createQuery(Podcast.class);
        Root<Podcast> p = query.from(Podcast.class);

        query
                .select(p)
                .where(
                        cb.equal(p.get("id"), id)
                );

        var podcast = em.createQuery(query).getSingleResult();

        return Optional.ofNullable(podcast);
    }
}
