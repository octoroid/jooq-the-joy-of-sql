package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.jdbc;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * Created by kevin on 11/07/2020
 */
public class PodcastRepository {

    private final Connection connection;

    public PodcastRepository(DataSource dataSource) throws SQLException { this.connection = dataSource.getConnection(); }

    public List<Podcast> findAll() throws SQLException {
        var stmt = connection.prepareStatement("""
                        SELECT ID, TITLE, URL
                        FROM PODCAST
                        ORDER BY ID ASC                        
        """);
        var rs = stmt.executeQuery();

        var podcasts = new ArrayList<Podcast>();

        while (rs.next()) {
            var id = rs.getObject("ID", UUID.class);
            var title = rs.getString("TITLE");
            var url = rs.getString("URL");

            podcasts.add(new Podcast(id, title, url));
        }

        rs.close();
        stmt.close();

        return podcasts;
    }

    public Optional<Podcast> findOneById(UUID id) throws SQLException {
        // Statement are String based and should be constructed
        // using only String methods (concat, join…)
        var stmt = connection.prepareStatement("""
                        SELECT TITLE, URL
                        FROM PODCAST
                        WHERE ID = ?              
        """);
        stmt.setObject(1, id); // Positional Parameters…
        var rs = stmt.executeQuery();

        rs.next(); // Managing spatial position cursor

        if (rs.isAfterLast()) {
            return Optional.empty();
        }

        var title = rs.getString("TITLE"); // Field name duplication
        var url = rs.getString("URL");     // for every one

        rs.close();     // Statement should be closed…
        stmt.close();   // And statement too

        return Optional.of(new Podcast(id, title, url));
    }

}
