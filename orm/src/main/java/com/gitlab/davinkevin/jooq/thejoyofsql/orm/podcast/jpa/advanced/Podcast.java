package com.gitlab.davinkevin.jooq.thejoyofsql.orm.podcast.jpa.advanced;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by kevin on 11/07/2020
 */
@NamedEntityGraph(
        name = "podcast-item-graph",
        attributeNodes = {
                @NamedAttributeNode("items")
        },
        subgraphs = {
                @NamedSubgraph(
                        name = "items-tags",
                        attributeNodes = {
                                @NamedAttributeNode("tags")
                        }
                )
        }
)
@Entity
@Table(name = "PODCAST", uniqueConstraints = @UniqueConstraint(columnNames={"id", "url"}))
public class Podcast {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "UUID")
    private UUID id;
    @Column(name = "TITLE")
    private String title;
    private String url;

    @OneToMany(mappedBy = "podcast")
    private List<Item> items = new ArrayList<>();

    public UUID getId() { return id; }
    public void setId(UUID id) { this.id = id; }

    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public String getUrl() { return url; }
    public void setUrl(String url) { this.url = url; }

    public List<Item> getItems() { return items; }
    public void setItems(List<Item> items) { this.items = items; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Podcast podcast = (Podcast) o;
        return Objects.equals(id, podcast.id) &&
                Objects.equals(title, podcast.title) &&
                Objects.equals(url, podcast.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, url);
    }
}
